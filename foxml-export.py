#!/usr/bin/env python3
# coding: utf-8

import argparse
from ch.memobase.export import foxml_export

arg_handler = argparse.ArgumentParser(description="Export all FOXML files for Fedora3 objectStore " +
                                                  "into record set specific folders withing OUTPUT-DIRECTORY")
arg_handler.add_argument("--object-store-directory", required=True)
arg_handler.add_argument("--output-directory", required=True)

args = arg_handler.parse_args()

foxml_export(args.object_store_directory, args.output_directory, "./record_sets_ids_dh_2.3.csv")
