#!/bin/bash

# Remove trailing slash, if present:
output_dir="${1%/}"

record_set_dir="$output_dir/snb-001"

echo "Fix XML tag technicalAttributeString in '$record_set_dir':"

sed -i 's/<technicalAttributeString typeLabel=""><\/technicalAttributeString>//g' "$record_set_dir/"*.xml
sed -i 's/<technicalAttributeString typeLabel="Format Arbeitskopie:/<technicalAttributeString typeLabel="Remarks">Format Arbeitskopie:/g' "$record_set_dir/"*.xml
sed -i 's/<technicalAttributeString typeLabel="Formato della copia di lavoro:/<technicalAttributeString typeLabel="Remarks">Formato della copia di lavoro:/g' "$record_set_dir/"*.xml
sed -i 's/<technicalAttributeString typeLabel="mp3-file /<technicalAttributeString typeLabel="Remarks">mp3-file /g' "$record_set_dir/"*.xml
# sed -i 's/"><\/technicalAttributeString>/<\/technicalAttributeString>/g' "$record_set_dir/"*.xml

echo "Done."
