#!/usr/bin/env python3
# coding: utf-8

import argparse
from ch.memobase.export import foxml_export, media_export
from os import listdir
from os import path

arg_handler = argparse.ArgumentParser(description="Perform a full export by first exporting all FOXML files " +
                                                  "and then exporting media files for each record set")
arg_handler.add_argument("--object-store-directory", required=True)
arg_handler.add_argument("--datastream-store-directory", required=True)
arg_handler.add_argument("--http-resources-directory", required=True)
arg_handler.add_argument("--rtmp-resources-directory", required=True)
arg_handler.add_argument("--sitemap-file", required=True)
arg_handler.add_argument("--output-directory", required=True)


args = arg_handler.parse_args()

foxml_export(args.object_store_directory, args.output_directory, "./record_sets_ids_dh_2.3.csv")

for record_set_directory in listdir(args.output_directory):
    record_set_path = path.join(args.output_directory, record_set_directory)
    if path.isdir(record_set_path):
        media_export(record_set_path, args.datastream_store_directory,
                     args.http_resources_directory, args.rtmp_resources_directory, args.sitemap_file)
