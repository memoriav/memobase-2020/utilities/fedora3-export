#!/usr/bin/env python3
# coding: utf-8

import argparse
from ch.memobase.export import media_export

arg_handler = argparse.ArgumentParser(description="Export media files for a specific record set")
arg_handler.add_argument("--record-set-directory", required=True)
arg_handler.add_argument("--datastream-store-directory", required=True)
arg_handler.add_argument("--http-resources-directory", required=True)
arg_handler.add_argument("--rtmp-resources-directory", required=True)
arg_handler.add_argument("--sitemap-file", required=True)

args = arg_handler.parse_args()

media_export(args.record_set_directory, args.datastream_store_directory,
             args.http_resources_directory, args.rtmp_resources_directory, args.sitemap_file)
