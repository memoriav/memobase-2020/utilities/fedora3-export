from os import path
from ch.memobase.foxml import FoxmlReader


class MediaFileSearcher:

    def __init__(self, foxml_reader: FoxmlReader, datastream_store_path, http_files_path, rtmp_files_path, sitemap):
        self.foxml_reader = foxml_reader
        self.datastream_store_path = datastream_store_path
        self.http_files_path = http_files_path
        self.rtmp_files_path = rtmp_files_path
        self.sitemap = sitemap

    def search_media_file(self):
        accesscopy_file = self.foxml_reader.get_accesscopy_datastream_file()
        locator = self.foxml_reader.get_locator()
        if accesscopy_file is not None:
            return self.__get_file_from_datastream_store(accesscopy_file, self.foxml_reader.get_accesscopy_datastream_original_filename())
        elif locator is not None:
            return self.__get_file_from_locator(locator)
        else:
            return self.__get_file_from_sitemap(self.foxml_reader.get_original_identifier())

    def search_thumbnail_file(self):
        thumbnail_file = self.foxml_reader.get_thumbnail_datastream_file()
        if thumbnail_file is not None:
            return self.__get_file_from_datastream_store(thumbnail_file, self.foxml_reader.get_thumbnail_datastream_original_filename())
        else:
            return None

    def __get_file_from_datastream_store(self, relative_file_path, original_filename):
        full_file_path = path.join(self.datastream_store_path, relative_file_path)
        return full_file_path, path.basename(original_filename)

    def __get_file_from_locator(self, locator):
        # search streaming resource
        if locator.startswith('https://memobase.ch/files/'):
            return self.__get_http_resource_file(locator)
        elif locator.startswith('rtmp://intstream.memobase.ch:1935/memobase/'):
            return self.__get_rtmp_resource_file(locator)
        else:
            return None

    def __get_http_resource_file(self, url):
        http_resource_path = path.join(self.http_files_path, url[len('https://memobase.ch/files/'):])
        if path.isfile(http_resource_path):
            return http_resource_path, path.basename(http_resource_path)
        else:
            return None

    def __get_rtmp_resource_file(self, url):
        rtmp_rel_path = url[len('rtmp://intstream.memobase.ch:1935/memobase/'):]
        source_filename = rtmp_rel_path[rtmp_rel_path.find(':') + 1:]
        source_path1 = path.join(self.rtmp_files_path, source_filename)
        source_path2 = path.join(self.rtmp_files_path, 'open', source_filename)

        if path.isfile(source_path1):
            return source_path1, path.basename(source_path1)
        elif path.isfile(source_path2):
            return source_path2, path.basename(source_path2)
        else:
            return None

    def __get_file_from_sitemap(self, original_document_id):
        url = self.sitemap.get(original_document_id)
        if url is not None:
            return self.__get_http_resource_file(url)
        else:
            return None
