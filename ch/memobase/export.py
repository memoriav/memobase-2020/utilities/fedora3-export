import logging
from glob import glob
from os import makedirs, path, walk
from shutil import copy2
from xml.dom.minidom import parse
from ch.memobase.media import MediaFileSearcher
from ch.memobase.records_sets import RecordSetIdMapper
from ch.memobase.foxml import FoxmlReader
from ch.memobase.foxml import FoxmlParsingError


def _copy_file(source_file, destination_directory, destination_filename, logger):
    if not path.exists(destination_directory):
        makedirs(destination_directory)

    destination_path = path.join(destination_directory, destination_filename)
    try:
        copy2(source_file, destination_path, follow_symlinks=False)
        logger.info("Exported file '" + source_file + "' to '" + destination_path + "'")
    except FileNotFoundError as not_found_error:
        logger.error("Error while copying file '" + source_file + "'", exc_info=not_found_error)


def _normalize_document_id(document_id):
    return document_id.replace("/", "_")


def _normalize_file_extension(original_file_name):
    ext = path.splitext(original_file_name)[1]
    return ext.lower()


def _is_image_file(file_name):
    image_file_extensions = [
        ".jpeg",
        ".jpg",
        ".png"
    ]

    normalized_file_ext = _normalize_file_extension(file_name)
    return normalized_file_ext in image_file_extensions


def _should_export_thumbnail(accesscopy_file):
    # only export thumbnail if accesscopy_file does not exist or is not an image file:
    return accesscopy_file is None or (accesscopy_file is not None and not _is_image_file(accesscopy_file[1]))


def _create_logger(name, logfile):
    # configure logger:
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(levelname)s: %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.ERROR)
    console_handler.setFormatter(formatter)

    file_handler = logging.FileHandler(logfile)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    return logger


def _parse_sitemap(sitemap_file: str) -> dict:
    sitemap = dict()
    dom = parse(sitemap_file)
    for url in dom.getElementsByTagName('url'):
        id = url.getElementsByTagName('originalId')[0].firstChild.data
        locator = url.getElementsByTagName('loc')[0].firstChild.data
        sitemap[id] = locator
    return sitemap


def foxml_export(objectstore_path, output_path, recordsets_csv_file):
    if not path.exists(output_path):
        makedirs(output_path)

    logger = _create_logger("foxml_export", path.join(output_path, "foxml_export.log"))

    recordset_id_mapper = RecordSetIdMapper(recordsets_csv_file)

    for r, d, f in walk(objectstore_path):  # r=root, d=directories, f = files
        for file in f:
            foxml_path = path.join(r, file)
            logger.debug("Parsing FOXML file: '" + foxml_path + "'")

            try:
                foxml_reader = FoxmlReader(foxml_path)
            except FoxmlParsingError as parsing_error:
                logger.error("Error while parsing FOXML file", exc_info=parsing_error)
            else:
                old_record_set_id = foxml_reader.get_recordset_identifier()
                if old_record_set_id is not None:
                    logger.debug("FOXML file " + foxml_path + " belongs to record set " + old_record_set_id)

                    if recordset_id_mapper.mapping_exists(old_record_set_id):
                        new_record_set_id = recordset_id_mapper.get_new_record_set_id(old_record_set_id)
                        record_set_export_path = path.join(output_path, new_record_set_id)
                        if not path.exists(record_set_export_path):
                            makedirs(record_set_export_path)
                        foxml_destination_path = path.join(record_set_export_path, path.basename(foxml_path) + ".xml")
                        copy2(foxml_path, foxml_destination_path, follow_symlinks=False)
                        logger.info("Exported FOXML file '" + foxml_path + "' to '" + foxml_destination_path + "'")
                    else:
                        logger.warning("Ignored FOXML file '" + foxml_path +
                                    "': Old recordset ID '" + old_record_set_id + "' not listed in 'record_sets_ids.csv'")
                else:
                    logger.warning("Ignored FOXML file '" + foxml_path +
                                   "': No recordset ID found in FOXML")

    print("Finished FOXML export")


def media_export(record_set_path, datastreamstore_path, http_files_path, rtmp_files_path, sitemap_file):
    logger = _create_logger("media_export", path.join(record_set_path, "media_export.log"))
    sitemap = _parse_sitemap(sitemap_file)

    for foxml_path in glob(path.join(record_set_path, "*.xml")):
        logger.debug("Exporting media files for file: '" + foxml_path + "'")

        foxml_reader = FoxmlReader(foxml_path)
        original_document_id = foxml_reader.get_original_identifier()

        media_file_searcher = MediaFileSearcher(foxml_reader, datastreamstore_path, http_files_path, rtmp_files_path,
                                                sitemap)

        accesscopy_file = media_file_searcher.search_media_file()
        if accesscopy_file is not None:
            _copy_file(accesscopy_file[0], path.join(record_set_path, "media"),
                       _normalize_document_id(original_document_id) + _normalize_file_extension(accesscopy_file[1]), logger)

        # only export thumbnail if accesscopy_file does not exist or is not an image file:
        if _should_export_thumbnail(accesscopy_file):
            thumbnail_file = media_file_searcher.search_thumbnail_file()
            if thumbnail_file is not None:
                _copy_file(thumbnail_file[0], path.join(record_set_path, "thumbnails"),
                           _normalize_document_id(original_document_id) + _normalize_file_extension(thumbnail_file[1]), logger)

    print("Finished media export for '" + record_set_path + "'")
