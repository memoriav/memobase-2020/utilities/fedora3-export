from csv import DictReader


class RecordSetIdMapper:

    def __init__(self, records_sets_file):
        self.__old2new = {}
        with open(records_sets_file, 'r') as csv_file:
            for row in DictReader(csv_file):
                old_id = row['identifier_old']
                new_id = row['identifier_new']
                self.__old2new[old_id] = new_id

    def mapping_exists(self, old_id):
        return old_id in self.__old2new

    def get_new_record_set_id(self, old_id):
        return self.__old2new[old_id]
