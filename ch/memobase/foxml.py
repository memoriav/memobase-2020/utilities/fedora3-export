from os import path
from xml.etree import ElementTree
from hashlib import md5
from urllib.parse import quote


def _get_element_text(start_element, xpath_expression):
    el = start_element.find(xpath_expression, FoxmlReader.NAMESPACES)
    if el is not None and el.text is not None:
        return el.text
    else:
        return None


def _get_attribute_value(start_element, element_xpath_expression, attribute_label):
    el = start_element.find(element_xpath_expression, FoxmlReader.NAMESPACES)
    if el is not None and el.attrib[attribute_label] is not None:
        return el.attrib[attribute_label]
    else:
        return None


def _get_last_element(start_element, xpath_expression, sort_by_attrib):
    elements = start_element.findall(xpath_expression, FoxmlReader.NAMESPACES)
    if len(elements) > 0:
        elements.sort(reverse=True, key=lambda elem: elem.attrib[sort_by_attrib])
        return elements[0]
    else:
        return None


def _get_metadata_datastream_element(root_element):
    return _get_last_element(
        root_element,
        "foxml:datastream[@ID='TRANSFORMED_METADATA_0']/foxml:datastreamVersion[@LABEL='Internal Memobase Metadata']",
        "CREATED")


def _get_thumbnail_datastream_element(root_element):
    return _get_last_element(
        root_element,
        ".//foxml:datastream[@ID='THUMBNAIL_0']/foxml:datastreamVersion",
        "CREATED")


def _get_accesscopy_datastream_element(root_element):
    return _get_last_element(
        root_element,
        ".//foxml:datastream[@ID='ACCESSCOPY_0']/foxml:datastreamVersion",
        "CREATED")


def _calculate_data_stream_path(datastream_id):
    full_id = "info:fedora/" + datastream_id.replace('+', '/')
    quoted_full_id = quote(full_id, safe='').replace('_', '%5F')
    hash_object = md5((full_id).encode())
    return path.join(hash_object.hexdigest()[0:2], quoted_full_id)


class FoxmlParsingError(Exception):
    pass


class FoxmlReader:
    NAMESPACES = {
        'foxml': 'info:fedora/fedora-system:def/foxml#',
        'oai_dc': 'http://www.openarchives.org/OAI/2.0/oai_dc/',
        'dc': 'http://purl.org/dc/elements/1.1/',
        'ebucore': 'urn:ebu:metadata-schema:ebuCore_2012',
        'ns2': 'http://purl.org/dc/elements/1.1/'
    }

    def __init__(self, file):
        root_element = ElementTree.parse(file).getroot()
        metadata_datastream_element = _get_metadata_datastream_element(root_element)
        if metadata_datastream_element is not None:
            self.metadata_datastream_element = metadata_datastream_element
            self.thumbnail_datastream_element = _get_thumbnail_datastream_element(root_element)
            self.accesscopy_datastream_element = _get_accesscopy_datastream_element(root_element)
        else:
            raise FoxmlParsingError("FOXML file '" + file + "' has no datastream with ID 'TRANSFORMED_METADATA_0'")

    def get_recordset_identifier(self):
        return _get_element_text(
            self.metadata_datastream_element,
            "foxml:xmlContent/ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:isMemberOf/ns2:relation")

    def get_main_identifier(self):
        return _get_element_text(
            self.metadata_datastream_element,
            "foxml:xmlContent/ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:identifier[@typeLabel='Main']/ns2:identifier")

    def get_original_identifier(self):
        return _get_element_text(
            self.metadata_datastream_element,
            "foxml:xmlContent/ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:identifier[@typeLabel='Original']/ns2:identifier")

    def get_locator(self):
        return _get_element_text(
            self.metadata_datastream_element,
            "foxml:xmlContent/ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:format/ebucore:essenceLocator/ebucore:locatorInfo")

    # def get_object_type(self):
    #     return _get_attribute_value(
    #         self.metadata_datastream_element,
    #         "foxml:xmlContent/ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:type/ebucore:objectType",
    #         "typeLabel")

    def get_accesscopy_datastream_original_filename(self):
        if self.accesscopy_datastream_element is not None:
            return self.accesscopy_datastream_element.attrib['LABEL']
        else:
            return None

    def get_accesscopy_datastream_file(self):
        if self.accesscopy_datastream_element is not None:
            accesscopy_ref = _get_attribute_value(self.accesscopy_datastream_element, "foxml:contentLocation", "REF")
            return _calculate_data_stream_path(accesscopy_ref)
        else:
            return None

    def get_thumbnail_datastream_original_filename(self):
        if self.thumbnail_datastream_element is not None:
            return self.thumbnail_datastream_element.attrib['LABEL']
        else:
            return None

    def get_thumbnail_datastream_file(self):
        if self.thumbnail_datastream_element is not None:
            thumbnail_ref = _get_attribute_value(self.thumbnail_datastream_element, "foxml:contentLocation", "REF")
            return _calculate_data_stream_path(thumbnail_ref)
        else:
            return None
